﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StringBuilderSqlCreator
{
    public partial class Form1 : Form
    {
        private bool IsTxtSqlChange = false;
        private bool IsTxtStringBuilderChange = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void txtSql_TextChanged(object sender, EventArgs e)
        {
            if (!IsTxtStringBuilderChange)
                IsTxtSqlChange = true;
            else
                return;

            var sqlLines = txtSql.Lines;

            var stringBuilder = "var sb = new StringBuilder();\r\n";
            foreach (var sqlLine in sqlLines)
            {
                stringBuilder = string.Format("{0}sb.Append(\" {1} \"); \r\n", stringBuilder, sqlLine);
            }

            txtStringBuilder.Text = stringBuilder;
        }

        private void txt_KeyDown(object sender, KeyEventArgs e)
        {
            if ((sender as TextBox) == txtSql)
                if (e.Control && e.Shift)
                {
                    if (e.KeyCode == Keys.U)
                    {
                        (sender as TextBox).SelectedText = (sender as TextBox).SelectedText.ToUpper();
                    }
                }

            IsTxtSqlChange = false;
            IsTxtStringBuilderChange = false;
        }

        private void txtStringBuilder_TextChanged(object sender, EventArgs e)
        {
            if (!IsTxtSqlChange)
                IsTxtStringBuilderChange = true;
            else
                return;

            var stringBuilderLines = txtStringBuilder.Lines;

            var sql = string.Empty;
            foreach (var stringBuilderLine in stringBuilderLines)
            {
                var lineAxu = stringBuilderLine.Replace("sb.Append(\"", "");
                lineAxu = lineAxu.Replace("\");", "");
                lineAxu = lineAxu.Replace("var sb = new StringBuilder();", "");
                lineAxu = lineAxu.Trim();

                sql += lineAxu + Environment.NewLine;
            }

            txtSql.Text = sql;
        }
    }
}
